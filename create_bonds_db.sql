DROP TABLE rating;
DROP TABLE bond;

CREATE TABLE bond
(
  ID INTEGER NOT NULL IDENTITY(1,1),
  CUSIP VARCHAR(12) NOT NULL,
  quantity INTEGER NOT NULL,
  price DECIMAL(12,4) NOT NULL,
  coupon DECIMAL(12,4) NOT NULL,
  maturity DATE NOT NULL,
  calldate DATE,
  rating VARCHAR(6),
  PRIMARY KEY (ID)
);

CREATE TABLE rating
(
  ordinal INTEGER NOT NULL,
  rating VARCHAR(6) NOT NULL,
  expected_yield DECIMAL(12,4) NOT NULL,
  PRIMARY KEY (ordinal)
);

INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('86840XH11',  50000 , 1.0325  , 0.0450  *100, '2025-01-01', '2021-01-01', 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('19085ED43',  10000 , 1.0105  , 0.0450  *100, '2022-10-01', '2020-10-01', 'AA1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('25471SC18',  20000 , 1.0540  , 0.0500  *100, '2028-06-15', '2023-06-15', 'BAA1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('92571UV41',  200000  , 1.0155  , 0.0425  *100, '2023-07-01', '2018-07-01', 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('01052VP37',  100000  , 0.9805  , 0.0180  *100, '2019-09-01', NULL, NULL);
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('28717RH95',  50000 , 1.0075  , 0.0325  *100, '2021-01-01', NULL, 'AA1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('81879OC55',  250000  , 1.0295  , 0.0325  *100, '2036-05-01', '2029-05-01', 'AAA');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('53303QP27',  60000 , 1.0035  , 0.0500  *100, '2029-12-01', '2022-12-01', 'BAA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('66648IU73',  75000 , 1.0100  , 0.0350  *100, '2024-04-01', '2020-04-01', 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('49902HQ45',  100000  , 1.0215  , 0.0425  *100, '2026-10-01', '2023-10-01', 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('48259ZK16',  50000 , 0.9775  , 0.0525  *100, '2024-07-01', '2019-07-01', 'BAA3');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('42702YH07',  10000 , 1.0270  , 0.0375  *100, '2025-08-01', '2021-08-01', 'AA1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('54798WQ09',  20000 , 1.0050  , 0.0400  *100, '2026-02-15', '2020-02-15', 'A1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('42726LS17',  200000  , 1.0465  , 0.0450  *100, '2028-03-15', '2022-03-15', 'AA1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('78199MH49',  100000  , 1.0450  , 0.0500  *100, '2034-03-01', '2027-03-01', 'A3');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('87180DE77',  50000 , 0.9950  , 0.0225  *100, '2020-05-15', NULL, NULL);
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('23549RB57',  250000  , 1.0375  , 0.0400  *100, '2029-10-01', '2025-10-01', 'AA3');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('23234SC00',  60000 , 1.0400  , 0.0375  *100, '2027-07-01', '2024-07-01', 'AAA');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('84363CP71',  75000 , 1.0225  , 0.0450  *100, '2022-04-01', NULL, 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('85143YR83',  100000  , 1.0275  , 0.0450  *100, '2024-06-15', '2019-06-15', 'A1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('61334WM19',  125000  , 1.0725  , 0.0488  *100, '2027-10-01', '2023-10-01', 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('32052GL43',  250000  , 1.0150  , 0.0450  *100, '2023-01-01', '2019-01-01', 'AA2');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('07601CI27',  60000 , 1.0377  , 0.0400  *100, '2026-08-15', '2021-08-15', 'A1');
INSERT INTO bond (CUSIP, quantity, price, coupon, maturity, calldate, rating) VALUES ('00375OR56',  75000 , 1.0025  , 0.0425  *100, '2021-12-01', NULL, 'AA1');

INSERT INTO rating (ordinal, rating, expected_yield) VALUES (1, 'AAA', 3.24);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (2, 'AA1', 3.28);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (3, 'AA2', 3.49);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (4, 'AA3', 3.7);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (5, 'A1', 4.01);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (6, 'A2', 4.1);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (7, 'A3', 4.26);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (8, 'BAA1', 4.8);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (9, 'BAA2', 4.98);
INSERT INTO rating (ordinal, rating, expected_yield) VALUES (10, 'BAA3', 5.07);
