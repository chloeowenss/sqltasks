-- Show all info about CUSIP stated
select * 
from bond
where CUSIP = '28717RH95'

-- Show all info about all bonds order from shortest
select * 
from bond 
order by maturity asc

--Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.
select sum(price), sum(quantity)
from bond

-- or 
select sum(quantity *price)
from bond

-- Annual return for each bond as the product of the quantity and the coupon
select coupon / 100 * quantity
from bond

-- Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. 
select * 
from bond 
where rating >= 'AA2' 

-- Show the average price and coupon rate for all bonds of each bond rating.
select avg(price), avg(coupon), bond.rating 
from bond
join rating on bond.rating = rating.rating
group by bond.rating 

-- Calculate the yield for each bond, as the ratio of coupon to price. Then, identify bonds that we might consider to be overpriced, as those whose yield is less than the expected yield given the rating of the bond.
select coupon/price yield, r.expected_yield 
from bond b
join rating r on r.rating = b.rating 
where (coupon/price) < r.expected_yield 


-- STRETCH GOALS -- 

-- show the degree of correlation between bond rating and coupo-n rate. Use ORD rating 

-- select b.coupon as c, 
-- r.ordinal as,
-- b.coupon * r.ordinal as total,
-- b.coupon * b.coupon as bc,
-- r.ordinal * r.ordinal as ro,
-- from bond b
-- join rating r on b.rating = r.rating; 

-- select (count(*) * sum(total - sum(c) * sum(o)) / 
 
