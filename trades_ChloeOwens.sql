-- Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.
select case when t.buy = 2 then 'buy' else 'sell' end, t.id, t.size, t.price, t.instant
from trade t 
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK'
order by t.instant asc

-- Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, minus the sum of the product of size and price for all buys.
select sum (t.size * t.price * (case when t.buy = 1 then -1 else 1 end))
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK' and p.closing_trade_id != p.opening_trade_id
and p.closing_trade_id is not null 

-- Develop a view that shows profit or loss for all traders.
create view trader_profits as 
select 
tr.first_name, tr.last_name,
sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'profit'
from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
join trader tr on p.trader_id = tr.id
where p.closing_trade_id is not null
and p.closing_trade_id != p.opening_trade_id
group by tr.first_name, tr.last_name
